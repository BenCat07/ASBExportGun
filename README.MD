# ASB Export Gun

**Credits for the Hashing Functionality go to Kozenomenon's [KozHash](https://github.com/Kozenomenon/KozHash/tree/master) Library**

**ModID:3004039936**


This Mod allows you to export a Dino for use with [Ark Smart Breeding (ASB)](https://github.com/cadon/ARKStatsExtractor), a tool which allows you to Import Dinos into a Library with Features such As Matching Dinos for Breeding, Tracking Hatch Timers and more.

This Mod aims to reduce the amount of work necessary for people to export a Dinos stats, as well as allowing you to also immediately open up a Rename Box if wanted, which combined with ASB's Automatic Naming Feature can allow very quick Naming of Dinos without having to Search through the Whole Library of Dinos.

## Configuration
All of the below go in GameUserSettings.ini
There are a Few Configuration Options to allow Server Owners to have Control over what is and what isn't allowed for Exporting. The Below Values are also the Defaults.
```ini
[ASBExportGun]
ExportGunRange=3000
AllowWildExport=Admin
AllowForeignDinoExport=Admin
AllowServerMultiplierExport=All
```

**`ExportGunRange`, Floating Point Number.**

Allows Changing the Range at which a Dino can be exported. Higher Values will allow Exports at further Distance, but may also cause more lag. If you want the Export Range to be Smaller, e.g. only allowing a Dino to be exported if you're very close to it, i recommend using about 600 as the range. 3000 Units is about 10 Foundations of Range, while 600 is 2 Foundations of Range.

**`AllowWildExport`, Choose from None, Admin, All**
| Option    | Effect |
| ------    | ------ |
| None      | Noone Will be able to Export Wild Dinos with the Tool.|
| Admin     | Only Admins Will be able to Export Wild Dinos Tool.|
| All       | Everyone Will be able to Export Wild Dinos Tool.|


**`AllowForeignDinoExport`, Choose from None, Admin, All**
| Option    | Effect |
| ------    | ------ |
| None      | Noone Will be able to Export Dinos Belonging to other Tribes and/or Alliance Members.|
| Admin     | Only Admins Will be able to Export Dinos Belonging to other Tribes and/or Alliance Members.|
| All       | Everyone Will be able to Export Dinos Belonging to other Tribes and/or Alliance Members.|



**`AllowServerMultiplierExport`, Choose from None, Admin, All**
| Option    | Effect |
| ------    | ------ |
| None      | The Servers Stat Multipliers are never Exported.|
| Admin     | The Servers Stat Multipliers are Exported for any admin using the Tool on a Dino.|
| All       | Everyone Will be able to Export The Stat Multipliers of the Server when using the Tool.|


## Blueprint Paths
All of these Go in the `Game.ini` Underneath the `[/script/shootergame.shootergamemode]` Header. If it already exists, just add the options below it :)
**`Engram Path`**: `EngramEntry_WeaponDinoExportGun_C`
Usage:
```ini
[/script/shootergame.shootergamemode]
OverrideNamedEngramEntries=(EngramClassName="EngramEntry_WeaponDinoExportGun_C",EngramPointsCost=3,EngramLevelRequirement=3)
```
Changes the Level Requirement to Level 3, and makes it require 3 Points to Craft.

**`Blueprint Path`**: `"Blueprint'/Game/Mods/DinoExportGun/Items/PrimalItem_WeaponDinoExportGun'"`
Usage:
```ini
[/script/shootergame.shootergamemode]
ConfigOverrideItemCraftingCosts=(ItemClassString="PrimalItem_WeaponDinoExportGun_C",BaseCraftingResourceRequirements=((ResourceItemTypeString="PrimalItemConsumable_RawMeat_C",BaseResourceRequirement=3.0,bCraftingRequireExactResourceType=False),(ResourceItemTypeString="PrimalItemConsumable_CookedMeat_C",BaseResourceRequirement=2.0,bCraftingRequireExactResourceType=False)))
```
Changes the Crafting Recipe to Cost 3 Raw Meat and 3 Cooked Meat. More details on that can be found unter [The Server Configuration Wiki Page](https://ark.wiki.gg/wiki/Server_configuration#ConfigOverrideItemCraftingCosts)


Here is the Same line but not as Ugly (You need it all to be on one line for the Game to recognize it though.):
```ini
ConfigOverrideItemCraftingCosts=(
    ItemClassString="PrimalItem_WeaponDinoExportGun_C",
    BaseCraftingResourceRequirements=(
      (ResourceItemTypeString="PrimalItemConsumable_RawMeat_C",
       BaseResourceRequirement=3.0,
       bCraftingRequireExactResourceType=False),
      (ResourceItemTypeString="PrimalItemConsumable_CookedMeat_C",
       BaseResourceRequirement=2.0,
       bCraftingRequireExactResourceType=False)
  )
)
```
 For other Customization, see [The Server Configuration Wiki Page](https://ark.wiki.gg/wiki/Server_configuration#OverrideEngramEntries_and_OverrideNamedEngramEntries)
 

### Why is this Mod Neccessary?
Currently, there are a total of 4 Ways to Export Dinos for use with External Programs such as ASB.

- Use the "Options" Menu on a Dino and use the "Export" Button.
- Manually Input the Numerical Values of the Dinos Stats.
- Use a Tool such as Super Spyglass to read the Points put into a given Stat.
- Save the World and import the whole Server Savegame.

The First problem is, **All** Of these options require that the server Owner gives the Client their Server's Stat Multipliers, and has to redistribute them every time they are modified. Alternatively the Client has to manually figure them all out, which is a headache for everyone involved, and often leads to having to visit the ASB Discord, posting the stats of Multiple Dinos, and waiting for someone to reverse the Exact Configuration for the Client.

Manually Inputting the Numerical Values of all of the Stats can be tiresome, and the "Export" option also cannot quite be trusted, as some Dinos have different stats Depending on if you just restarted the Server, found them in the Wild, or put them in a cryopod before releasing them. Imprints also do not update unless you cryo and uncryo a Dino, and it makes the lives of Breeders annoying if they don't have direct Access to the Actual Stat Points.

Using Super Spyglass and such is a good Middle point, but you have to either rely on Screen Capture to automatically detect the Numbers, or Manually Type in all the Stats.

The Last option is the most complete, but it would allow Every Client to also view other Tribe's Dinos, and even find out which Dinos have died and such. Definitely not appropriate for a PvP Setting, and some sort of System has to be set up so Clients actually get Access to the Save game, which is often impossible in a secure way unless you host the server on your own Machine.


**Meanwhile the Export Tool allows you to just Left-click a Dino, You get all of the Dinos Stats, Levels, and also get the Multipliers necessary for Immediately Working with ASB.** The Owner of the Server installs this mod, and never has to worry about a Breeder asking for Multipliers. The Tool also Fixes a lot of issues with the Normal Exports, as it properly Syncs the Stats, and exports it in a Custom Format that ASB can read. It additionally also exports it just like the "Options" -> "Export Dino" on the Dino itself, except the Stats are properly synced, and you don't have to open up the Ancestry Menu to get Ancestry data to show up.


**TL;DR**
Normal Exports have a lot of Issues that add up to a Big Headache. Especially for Server Owners, as they are likely asked for Stat Multipliers. With this Mod, a Player can instead just Left-click a Dino with the Tool and get all the Information they need exported.



Current Workshop Link:
https://steamcommunity.com/sharedfiles/filedetails/?id=3004039936